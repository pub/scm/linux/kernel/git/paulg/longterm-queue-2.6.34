This was the repository for the commits used in linux-2.6.34.y
stable releases v2.6.34.10 --> v2.6.34.15 inclusive.

The applied and tagged patches can be found here:
   https://git.kernel.org/cgit/linux/kernel/git/stable/linux-stable.git/log/?h=linux-2.6.34.y

Note that as of v2.6.34.15, the maintenance of v2.6.34.x has ended
and this version is now officially EOL.

So there will be no more commits in this repository; it is
now only of historical value.

					PaulG 02/2014
