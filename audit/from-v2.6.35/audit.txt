---- v2.6.35.1 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
3f3ed157 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
4060b482 	a1efd14a	v2.6.36		y	34.3 0f6b65e3
50783042 	cda4b7d3	v2.6.36		n
c1899005 	e06b14ee	v2.6.36		n
44437579 	812d0469	v2.6.36		y	34.3 14ccd3c0
1e4966cc 	e376573f	v2.6.36		n
37aa5ce0 	1297c05a	v2.6.36		y	34.3 41673b54
a1e0ea5d 	8a22b999	v2.6.36		y	34.3 9967f081
efe5d7f1 	a91c1be2	v2.6.36		y	34.3 439dd83a
5232dfac 	694f690d	v2.6.36		x	only for >=35
1a0528f5 	c9370197	v2.6.36		y	34.3 6f3748cc
bfcd88d2 	ccb6c136	v2.6.36		y	34.3 d3ae12ac
89b15fbb 	643f82e3	v2.6.36		y	34.3 512a9dbf
433a64e4 	9d1ac34e	v2.6.36		y	34.3 8235ddc2
5806c044 	d28232b4	v2.6.36		y	34.3 9c9dc3c4
50c14418 	601e0cb1	v2.6.36		n	drop ar9003 bits
b2ca8ba2 	4cee7861	v2.6.36		y	34.3 2fb49ae6
98757bba 	f860d526	v2.6.36		y	34.3 a1972d6d
793efc36 	31e79a59	v2.6.36		x	only for >=36-rc1
caaec883 	73e19463	v2.6.36		n	req. 1beb676d
a9328d50 	78c4653a	v2.6.36		n
6554c277 	03b4776c	v2.6.36		y	34.3 8e0e0465
52166187 	23399016	v2.6.36		n/a	(file -EEXIST)
5776fe6e 	9cc2f3e8	v2.6.36		n/a	(req 9b9cc61c,35rc1)
94230128 	2b40994c	v2.6.36		y	34.3 dd6f960a
8e5ad4d8 	4c85ab11	v2.6.36		y	34.3 b8122172
21b2bf38 	5fa8517f	v2.6.36		y	34.3 838f066e
edea2ace 	5c4bfa17	v2.6.36		y	34.3 7ac2eb70
0c41742f 	ff847ac2	v2.6.36		y	34.3 8d7738fa
a436d816 	36f2407f	v2.6.36		y	34.3 309acb84
2d8acc42 	59297067	v2.6.36		y	34.4 f2c47190
9bfcb1f8 	06c4648d	v2.6.36		y	34.4 ae6e1025
69e18d28 	962b70a1	v2.6.36		y	34.3 36e6db02
1c628740 	bc571178	v2.6.36		y	34.3 4895eb4c
8566b8f5 	9975a5f2	v2.6.36		y	34.3 b506d288
5a847787 	d9b68e5e	v2.6.36		y	34.3 b2a9b4fa
acdc7083 	e75aa858	v2.6.36		n/a	only for 35.
2812c8f7 	14cb0deb	v2.6.36		y	34.3 472d8431
01212f0b 	4b4fd27c	v2.6.36		y	34.3 3c9cb324
---- v2.6.35.2 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
f6b0f3c6 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
76e3b8d9 	96054569	v2.6.36		y	34.4 e320cef8
3aba3fa0 	5528f913	v2.6.36		y	34.4 5eafdadb
52423b90 	320b2b8d	v2.6.36		y	34.4 ca2f9017
96fbd3f2 	437f88cc	v2.6.36		y	in queue already
c1e592f0 	0b767f96	v2.6.36		n
925a7e4a 	4877c737	v2.6.36		y	34.4 fccf1002
88154b0e 	685fd0b4	v2.6.36		y	34.4 ac013b45
488a52f0 	38117d14	v2.6.36		y	34.4 a8121b31
60bee7a8 	aca0fa34	v2.6.36		y	34.4 498e2004
ba87b9af 	4565956d	v2.6.36		n/a	not for < 35 [l2tp]
a456e2bf 	81cbb0b1	v2.6.36		y	34.4 1dfb38cd
01806c2c 	6a8cfcfb	v2.6.36		n/a	NAND_CMD_RESET -EEXIST
1dc89aec 	396e894d	v2.6.36		n/a	nohz_ratelimit -EEXIST
b8253929 	0a79f674	v2.6.36		y	34.4 0586073c
4a561f2e 	b58af406	v2.6.36		n/a	code too different
8762dc28 	b3e67044	v2.6.36		y	34.4 ba4fc9c2
733677e1 	e10e1bec	v2.6.36		y	34.4 150efe00
9a03bd19 	356c5a48	v2.6.36		y	34.4 fe4776f6
ebc56bdd 	b6180ef7	v2.6.36		y	34.4 a2b5c888
ee567603 	0372a754	v2.6.36		y	34.4 e3792e0d
13ffcb8f 	b972302b	v2.6.36		y	34.4 07961759
a553041b 	afad1964	v2.6.36		y	34.4 eca0a43e
9bb40fbf 	93362a87	v2.6.36		y	34.4 631b2d37
064b8f10 	33d973ad	v2.6.36		y	34.4 df9f1d0d
a6b37a9e 	ae68a83b	v2.6.36		y	34.4 517ba4f6
63d0b4fd 	0936fb5e	v2.6.36		n/a	file -EEXIST < 35rc1
5605a6f7 	868003ca	v2.6.36		y	34.4 8c4db5ab
2997ccb8 	cdf357f1	v2.6.36		y	34.4 ca928dcb
3e3c8c71 	492c5d94	v2.6.36		y	34.4 35394fdc
c53f89ac 	a6cd7eb3	v2.6.36		y	34.4 76599ef9
d774a312 	a2a20c41	v2.6.36		y	34.4 e9d2a41c
bd388a58 	aca27ba9	v2.6.36		y	34.4 e720aace
cf6fdb61 	6710a576	v2.6.36		n/a	code -EEXIST
d67a13b5 	85f4cc17	v2.6.36		n
8a7da74a 	ceeab929	v2.6.36		y	34.4 f70877be
a672b731 	31f73bee	v2.6.36		y	34.4 c0eb70bb
46c7e62e 	c43f7b8f	v2.6.36		y	34.4 8939a755
eff3960f 	b7300b78	v2.6.36		y	34.4 ac793824
752638f5 	7a4dec53	v2.6.36		n
252eca4a 	556ab45f	v2.6.36		n
5dd8e4f3 	2d0bb1c1	v2.6.36		n
f986737b 	51e9ac77	v2.6.36		y	34.4 f835fa44
8fa8b10c 	bb4f1e9d	v2.6.36		n
3b9f17f7 	147e0b6a	v2.6.36		n/a	only for >=35rc1
8a4ddef0 	11071282	v2.6.36		y	34.4 fc20cc6e
3821841d 	d9e1b6c4	v2.6.36		n/a	only for >=35
d3c5501e 	8f1d2d2b	v2.6.36		n/a	no beacon_work in 34
563fd083 	e95b7435	v2.6.36		y	34.4 ce8be319
ef5cd7c1 	fa260c00	v2.6.36		y	34.4 dcd45d5f
c5e1a7d5 	966cca02	v2.6.36		y	34.4 8fe60a87
69348a6d 	bf9c1fca	v2.6.36		y	34.4 8eab907a
8642acfa 	ee78bb95	v2.6.36		y	34.4 bf7ce029
a88549a4 	e096c8e6	v2.6.36		n/a	no beep list in 34
f38a529f 	68f202e4	v2.6.36		n/a	no stop_one_cpu_nowait
9e90e744 	1f6ea6e5	v2.6.36		y	34.4 4eb2bc52
ecfd2b16 	549e1561	v2.6.36		y	34.4 da95717e
692f3d4a 	3d2a5318	v2.6.36		y	34.4 9b748c86
855b9e05 	e847003f	v2.6.36		n
570d6f5c 	54d2379c	v2.6.36		n/a	no dep on LBADF in 34
f4a92a35 	060132ae	v2.6.36		y	34.4 392e6701
f224c683 	e928c077	v2.6.36		y	34.4 04a22f95
8f0bb9ae 	5d92fe33	v2.6.36		y	34.4 79853f1e
56813262 	113fc5a6	v2.6.36		y	34.4 2d0c3a0a
203824be 	127c03cd	v2.6.36		y	34.6 e9237ad3
4fbb306c 	2491762c	v2.6.36		y	34.4 1c843b58
97537a75 	8b8f79b9	v2.6.36		y	34.4 16103e9d
afaac258 	e32e78c5	v2.6.36		y	34.4 aadd8971
eca5c118 	69e77a8b	v2.6.36		n/a	fix for >=35
da2756b5 	213373cf	v2.6.36		y	34.4 0fed784e
94f6da84 	9f242dc1	v2.6.36		y	34.4 edbd662b
---- v2.6.35.3 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
6d23f508 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
19b94a73 	xxxxxxxx	-- ? -- 	?	n/a for 34
44768880 	d7824370	v2.6.36		y	34.5 57cbde5b
16af977d 	11ac5524	v2.6.36		y	34.5 4def7cec
---- v2.6.35.4 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
1506707a 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
bdf6e8eb 	ede1b429	v2.6.36		y	34.6 4c0d125b
f5d76aba 	9d0f4dcc	v2.6.36		n
a87da791 	ea233f80	v2.6.36		y	34.6 44325f6b
a305c4c5 	666cc076	v2.6.36		y	34.6 173e878e
ba6da120 	a1669b2c	v2.6.36		y	34.6 b9b1463a
e5af0950 	0827a9ff	v2.6.36		y	34.6 aced8781
5ce775be 	d1ab903d	v2.6.36		y	34.6 233f1043
cdead9a3 	72916791	v2.6.36		y	34.6 35bc803a
a848012a 	f36ecd5d	v2.6.36		y	34.6 413a44ce
e60577f8 	0eee6a2b	v2.6.36		y	34.6 03e435a9
51f663a0 	76078dc4	v2.6.36		y	34.6 b8191910
680dc8e5 	c686ecf5	v2.6.36		y	34.6 954c1c9b
964505fa 	f86b9984	v2.6.36		n
b6c68ad8 	51a00eaf	v2.6.36		n
6e5cab8e 	1189f130	v2.6.36		n
37b419b2 	9abc1023	v2.6.36		n
cbdd4c09 	b9f0aee8	v2.6.36		y	34.6 e72ac085
e617eefb 	2cbeb4ef	v2.6.36		y	34.6 83573f5b
1cc67a62 	4b80d954	v2.6.36		y	34.6 3b56b9bb
410ff49b 	5786e2c5	v2.6.36		y	34.6 e76cb6d8
a9982269 	0537398b	v2.6.36		y	34.6 58f3ca3a
0e485a9e 	4e186b2d	v2.6.36		n
eeba6084 	da7be684	v2.6.36		y	34.6 f2e0d486
185726c2 	68d6ac6d	v2.6.36		y	34.6 73034307
e7d2aabc 	9c77b846	v2.6.36		y	34.6 6325f146
ca3d0a0f 	3c955b40	v2.6.36		y	34.6 ef84ac9d
63795834 	8b8ab9d5	v2.6.36		n
654cfa23 	1aef70ef	v2.6.36		y	34.6 bf2d40dd
d94b0aa7 	19833b5d	v2.6.36		y	34.6 55bfc176
305b06f7 	3f77316d	v2.6.36		n
3ee378d7 	94597ab2	v2.6.36		n
a5f18b1c 	cff0d6e6	v2.6.36		n
59ea469c 	c7dcf87a	v2.6.36		n
0d3a7582 	deda2e81	v2.6.36		n
516125a1 	d1d6ca73	v2.6.36		n
e29eaa0c 	a7c55cbe	v2.6.36		y	34.6 11ea3cff
fb862f23 	45c34e05	v2.6.36		y	34.6 3cbb7587
83b79480 	7e27a0ae	v2.6.36		y	34.6 fea4ed49
9a2faa7b 	7d060ed2	v2.6.36		y	34.6 002e54a6
1cfe75af 	01cd2aba	v2.6.36		n
ea554ee3 	31d1d48e	v2.6.36		n
1cd65f87 	072d79a3	v2.6.36		n
1378008c 	cece1945	v2.6.36		n
34793c34 	41065fba	v2.6.36		y	34.6 040862f9
0ff3da3b 	eb4a5527	v2.6.36		y	34.6 30b50aef
4c0ef2f2 	e5093aec	v2.6.36		y	34.6 7abab52d
525127bf 	ce9e76c8	v2.6.36		y	34.6 700aaace
b46ddc15 	4b030d42	v2.6.36		y	34.6 9ee75e1b
1a1b1d29 	5b75c497	v2.6.36		y	34.6 2afc67ed
51a9e83a 	3a3dfb06	v2.6.36		y	34.6 c57bbdaf
1fa22dac 	6d1d1d39	v2.6.36		y	34.6 047e9f3e
8eb6fff6 	a3bdb549	v2.6.36		y	34.6 77c043b1
f49fd7f7 	eeaf61d8	v2.6.36		y	34.6 73be303e
922ab18b 	0a492896	v2.6.36		n
bbb0cf2f 	86fa04b8	v2.6.36		y	34.6 9ddbf271
e214c09d 	ef201beb	v2.6.36		y	34.6 1900a7a3
ef0b9559 	c8837434	v2.6.36		n	parent wrong in 35
5ef58523 	1ab335d8	v2.6.36		y	34.6 7fbb3878
1f0a86af 	69309a05	v2.6.36		y	34.6 e77b85ce
6bed9991 	0e8e50e2	v2.6.36		y	34.6 7ddb5ec3
e89cfc48 	7798330a	v2.6.36		y	34.6 259756fc
f5981434 	297c5eee	v2.6.36		y	34.6 159e7674
d97eabc0 	98f33285	v2.6.36		y	34.6 fbc69b9c
c51ca794 	abdc568b	v2.6.36		y	34.6 97159b73
d59e0c5a 	c2411045	v2.6.36		y	34.6 1ad88d51
72c7a360 	1e5554c8	v2.6.36		y	34.6 912fd889
4f2624fb 	5ddb954b	v2.6.36		y	34.6 71e55b36
8a8c25a7 	6146b3d6	v2.6.36		y	34.6 46e21869
aa6b6a4b 	69d0b96c	v2.6.36		y	34.6 d9397f51
6eb22281 	8ae66418	v2.6.36		n
01be8ecc 	cfe3fdad	v2.6.36		n
e5c9bc34 	c81476df	v2.6.36		y	34.6 d1061f30
f50b2280 	93b352fc	v2.6.36		y	34.6 ecc2af8b
18626abc 	6ccf15a1	v2.6.36		y	34.6 a47320b6
31e682b1 	0a377cff	v2.6.36		n
286ba0c3 	9b00c643	v2.6.36		y	34.6 e5f71959
b97ef8ac 	da93f106	v2.6.36		n
6a7aa719 	ef56609f	v2.6.36		y	34.6 dce48ca9
665de5aa 	ef077179	v2.6.36		y	34.6 88833f82
01d74006 	41e2e8fd	v2.6.36		y	34.6 3da814e9
9695add7 	b9783dce	v2.6.36		y	34.6 7b355030
3770619e 	d8ab3557	v2.6.36		n
03dc7fc1 	9ea2c4be	v2.6.36		y	34.6 8ee8efcb
53aaf5f2 	845b6cf3	v2.6.36		y	34.6 a0f9476e
236550e7 	05e40760	v2.6.36		y	34.6 f76958c1
05a93944 	351af072	v2.6.36		n
6105020a 	d7c53c9e	v2.6.36		y	34.6 858ba8a4
7e817098 	c3f755e3	v2.6.36		y	34.6 a1c9f646
7811db7b 	5989cd6a	v2.6.36		n
e77930c7 	1c250d70	v2.6.36		n
edda4cb2 	30246557	v2.6.36		n
57001f22 	fe100acd	v2.6.36		y	34.6 be0fb5aa
19d3c467 	18fab912	v2.6.36		y	34.6 118fa535
ee384c27 	575570f0	v2.6.36		y	34.6 19349f0e
3ab47fa1 	af4e3631	v2.6.36		y	34.6 28d07211
208348a4 	fe0dbcc9	v2.6.36		y	34.6 e02d3555
7b5b07e3 	b11f1f1a	v2.6.36		y	34.6 c93e8d31
d79caba7 	a524812b	v2.6.36		y	34.6 1dbe8a5b
6d4ff3b0 	8a2e70c4	v2.6.36		y	34.6 bc8a22fb
caee59ec 	7beaf243	v2.6.36		y	34.6 5509b7d7
3730039f 	6d98c3cc	v2.6.36		y	34.6 ea156ca4
15ddf0fc 	6eda3dd3	v2.6.36		y	34.6 9c03fdac
94c25715 	c3e68fad	v2.6.36		y	34.6 b7377866
a41c86f2 	53bacfbb	v2.6.36		y	34.6 44aca463
2f10b292 	56385a12	v2.6.36		y	34.6 f7b4b640
6c877dfb 	a5ba6beb	v2.6.36		y	34.6 c8217fce
4e354ad4 	c4604e49	v2.6.36		y	34.6 4f8f5073
a4e5c5d3 	ac770267	v2.6.36		y	34.6 a09c72a3
f62010c9 	b2c1e07b	v2.6.36		y	34.6 aed52559
1360aa27 	4f0ed9a5	v2.6.36		y	34.6 ba7c6641
d3ad3eb0 	0a7992c9	v2.6.36		n
e9eee0b4 	d862b13b	v2.6.36		y	34.6 ff794f11
da6827e8 	21fd0495	v2.6.36		y	34.6 7db3ade7
---- v2.6.35.5 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
db49cf20 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
d3ad7717 	356ad3cd	v2.6.36		y	got from 32-stable
7e8ceff0 	12e8ba25	v2.6.36		n
d9a9ffcc 	dd8849c8	v2.6.36		n
2a0726e8 	032d2a0d	v2.6.36		y	got from 32-stable
d3899a16 	9f82d238	v2.6.36		n
3eef1e7f 	a25c25c2	v2.6.36		n
1c6fd63c 	4f7f7b7e	v2.6.36		n
9ee31388 	c877cdce	v2.6.36		y	got from 32-stable
8ea422ed 	9927a403	v2.6.36		y	got from 32-stable
6a32943a 	b741be82	v2.6.36		n
262a57c0 	7e7b41d2	v2.6.36		n
314a41c5 	95347871	v2.6.36		n
af38a70d 	f90087ee	v2.6.36		n
8fa16293 	0d9958b1	v2.6.36		n
b04d80ba 	a69ffdbf	v2.6.36		n
0e4112f0 	87cbf8f2	v2.6.36		n
0b6945bf 	df51e7aa	v2.6.36		n
661b2c51 	5a67657a	v2.6.36		y	got from 32-stable
5c416280 	b20d37ca	v2.6.36		y	got from 32-stable
801c1aca 	460cf341	v2.6.36		n
1a5029d5 	1d220334	v2.6.36		y	got from 32-stable
12c5db34 	c3b327d6	v2.6.36		y	got from 32-stable
75455a54 	96f36408	v2.6.36		y	got from 32-stable
dd32c724 	f17c811d	v2.6.36		n
9f6ea8d2 	653d48b2	v2.6.36		y	got from 32-stable
e8ba36a9 	54ff7e59	v2.6.36		n
1f4536a2 	145a902b	v2.6.36		n
428b0123 	eefdca04	v2.6.36		y	got from 32-stable
b2e19c77 	c41d68a5	v2.6.36		y	got from 32-stable
0fd7b7cb 	36d001c7	v2.6.36		y	got from 32-stable
51fccd33 	55496c89	v2.6.36		y	got from 32-stable
f155ee1b 	dc4e96ce	v2.6.36		n
53d9dee0 	42da2f94	v2.6.36		y	got from 32-stable
39fc0591 	d8e1ba76	v2.6.36		y	got from 32-stable
13d7e89e 	f880c205	v2.6.36		y	got from 32-stable
f4d60f54 	b0d278b7	v2.6.36		n
c0016ec4 	5225c458	v2.6.36		y	got from 32-stable
1d1a528b 	0dcc48c1	v2.6.36		y	got from 32-stable
ac1a91ed 	1ca56e51	v2.6.36		n
e9d3ad84 	af045b86	v2.6.36		y	got from 32-stable
964c3f00 	ee3aebdd	v2.6.36		y	got from 32-stable
9eca1e9d 	1c24de60	v2.6.36		y	got from 32-stable
5b253de7 	bc693045	v2.6.36		n
cfa7e5c5 	ac8456d6	v2.6.36		y	got from 32-stable
b4985378 	eee743fd	v2.6.36		n
bcedd390 	fbf3fdd2	v2.6.36		n
e921923e 	81ca03a0	v2.6.36		n
094b59b2 	4c2ef25f	v2.6.36		n
77dc2884 	5600efb1	v2.6.36		y	got from 32-stable
99e2e179 	b78d6c5f	v2.6.36		y	got from 32-stable
b2f62d51 	85a0fdfd	v2.6.36		y	got from 32-stable
8417544f 	4e70598c	v2.6.36		n
edcc1714 	cf9b94f8	v2.6.36		y	got from 32-stable
1b8c931c 	df091625	v2.6.36		y	got from 32-stable
3c3d2da7 	9c55cb12	v2.6.36		y	got from 32-stable
b8536db2 	3aaba20f	v2.6.36		y	got from 32-stable
803ac2f1 	7a801ac6	v2.6.36		n
2b88bc4f 	f1f5a807	v2.6.36		n
63b75413 	40c60230	v2.6.36		n
ff4adc0b 	e2f3d75f	v2.6.36		y	got from 32-stable
25de680b 	c29771c2	v2.6.36		n
b68ccacf 	269f45c2	v2.6.36		y	got from 32-stable
ad8096c8 	10f0412f	v2.6.36		y	got from 32-stable
3f0cfc09 	750d857c	v2.6.36		y	got from 32-stable
a319dbda 	57f9bdac	v2.6.36		y	got from 32-stable
697c5a83 	8f2ae0fa	v2.6.36		n
eb5b5fb7 	33994466	v2.6.36		n
b77c254d 	b73d7fce	v2.6.36		n
ac6042dd 	9cf2657d	v2.6.36		n
7a2a632d 	7b6717e1	v2.6.36		n
e46eb17f 	122661b6	v2.6.36		n
e9073fb9 	a769cbcf	v2.6.36		n
511ff6d6 	4d155641	v2.6.36		n
e45ef8fb 	76195fb0	v2.6.36		n
7ab99e2e 	27f7ad53	v2.6.36		y	got from 32-stable
0aa2a836 	fe6ce80a	v2.6.36		n
62f520f1 	4c25b932	v2.6.36		n
a8ff7995 	a2acad82	v2.6.36		n
a6a72ec7 	048e78a5	v2.6.36		n
f723b548 	577045c0	v2.6.36		y	got from 32-stable
67584f5e 	5b239f0a	v2.6.36		y	got from 32-stable
7b4275fe 	4035e456	v2.6.36		y	got from 32-stable
aef30a0b 	902ffc3c	v2.6.36		n
652d6da0 	e950598d	v2.6.36		n
d2d14437 	870408c8	v2.6.36		y	got from 32-stable
0679cf82 	caf3a636	v2.6.36		y	got from 32-stable
6b0ad10a 	65737388	v2.6.36		y	got from 32-stable
91ffbb4a 	0791971b	v2.6.36		n
a1af0d0d 	0bf7a81c	v2.6.36		y	got from 32-stable
55772a66 	541e05ec	v2.6.36		y	got from 32-stable
f813a03c 	037d3656	v2.6.36		y	got from 32-stable
654931d5 	08a3b3b1	v2.6.36		y	got from 32-stable
ab36a5f7 	33674691	v2.6.36		n
1e34225f 	f5ce5a08	v2.6.36		y	got from 32-stable
5f555d8a 	90487974	v2.6.36		y	got from 32-stable
5db688f4 	803288e6	v2.6.36		n
f8d958a9 	71ba186c	v2.6.36		n
58b29725 	071249b1	v2.6.36		n
b7a83862 	595afaf9	v2.6.36		y	got from 32-stable
eddbe634 	77c5ceaf	v2.6.36		y	got from 32-stable
bac51cb8 	15dd1c9f	v2.6.36		y	got from 32-stable
7d98d6ff 	e5fa721d	v2.6.36		y	got from 32-stable
0dfc7206 	0c47a70a	v2.6.36		y	got from 32-stable
92043815 	b681b588	v2.6.36		y	got from 32-stable
738931dd 	9e693e43	v2.6.36		n
34096073 	b08b1637	v2.6.36		y	got from 32-stable
96f44a86 	5d4abf93	v2.6.36		y	got from 32-stable
e3086e9d 	6e49c1a4	v2.6.36		n
96d55caa 	fb511f21	v2.6.36		n
f2b23f0f 	5b3ff237	v2.6.36		n
1ee3a461 	40e2e973	v2.6.36		n
339cdb04 	30da5524	v2.6.36		y	got from 32-stable
fa4dc90c 	fcd097f3	v2.6.36		y	got from 32-stable
aa7fe1f4 	cca77b7c	v2.6.36		n
133aa6cb 	8d330919	v2.6.36		n
de9711df 	cd7240c0	v2.6.36		y	got from 32-stable
6d09dba3 	546a1924	v2.6.36		n
480a3d4a 	aba8a08d	v2.6.36		n
004c068d 	44b73380	v2.6.36		y	got from 32-stable
53c51d4a 	55ee67f8	v2.6.36		n
d5182621 	150b432f	v2.6.36		y	got from 32-stable
ba106dfd 	81cd3fca	v2.6.36		n
5beda62a 	dbbcbc07	v2.6.36		n
efa1f7cb 	5b3eed75	v2.6.36		n
cf4f5cec 	4536f2ad	v2.6.36		n
fb412a17 	aaca4964	v2.6.36		y	got from 32-stable
62b1700a 	dffe2e1e	v2.6.36		y	got from 32-stable
44278346 	a05e93f3	v2.6.36		y	got from 32-stable
b3f69c04 	c12c507d	v2.6.36		n
---- v2.6.35.6 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
7273c1c6 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
97fe34b1 	3e073367	v2.6.36		y	got from 32-stable
ae043da9 	89749350	v2.6.36		n
21a32e67 	4bdab433	v2.6.36		y	got from 32-stable
2178c26d 	f362b732	v2.6.36		y	got from 32-stable
0ac99e6f 	8ca3eb08	v2.6.36		y	got from 32-stable
3c8b7588 	72853e29	v2.6.36		y	got from 32-stable
87f1cbde 	aa454840	v2.6.36		y	got from 32-stable
39f7a62c 	9ee493ce	v2.6.36		y	got from 32-stable
221d4da7 	d5164dbf	v2.6.36		n
92e8e73d 	59b25ed9	v2.6.36		n
013dbea4 	1b0e372d	v2.6.36		n
24dadf6e 	417484d4	v2.6.36		n
ff278af4 	6715045d	v2.6.36		n
2af3495b 	152e1d59	v2.6.36		n
bf818d94 	8d2602e0	v2.6.36		y	got from 32-stable
63e70525 	b4aaa78f	v2.6.36		y	got from 32-stable
0d1f22d2 	a122eb2f	v2.6.36		y	got from 32-stable
0f403655 	3d96406c	v2.6.36		y	got from 32-stable
21313e07 	9d1ac65a	v2.6.36		y	got from 32-stable
f2c26f52 	2d2b6901	v2.6.36		y	got from 32-stable
749d278c 	f574c843	v2.6.36		y	got from 32-stable
d963ca2b 	3444d7da	v2.6.36		y	got from 32-stable
291e0641 	6e3e243c	v2.6.36		n
41d2a3bc 	c0e0608c	v2.6.36		n
724b86b0 	9e7b0e7f	v2.6.36		y	got from 32-stable
aaf270ed 	7ac77099	v2.6.36		n
1be50198 	e36d96f7	v2.6.36		n
80e1809c 	2a1b7e57	v2.6.36		y	got from 32-stable
f4254199 	611da04f	v2.6.36		y	got from 32-stable
b85ec83e 	615661f3	v2.6.36		n
2e4e3bb7 	5f487490	v2.6.36		n
06a35185 	751ae808	v2.6.36		n
1d506ee4 	75e1c70f	v2.6.36		y	got from 32-stable
94c19579 	a0c42bac	v2.6.36		n
3cc932ba 	46b30ea9	v2.6.36		y	got from 32-stable
ee71a393 	d1908362	v2.6.36		n
db42f7e4 	c227e690	v2.6.36		n
32e987bf 	767b68e9	v2.6.36		n
e658aee2 	f501ed52	v2.6.36		n
a3bec3ef 	fd02db9d	v2.6.36		y	got from 32-stable
e675865c 	2aeadc30	v2.6.36		n
b932db65 	df08cdc7	v2.6.36		y	got from 32-stable
0f8f6597 	371d217e	v2.6.36		y	got from 32-stable
2263d443 	976e48f8	v2.6.36		n
e1d75198 	41a51428	v2.6.36		n
6d4179df 	c33f543d	v2.6.36		y	got from 32-stable
22bad8bb 	e75e863d	v2.6.36		y	got from 32-stable
7495973b 	950eaaca	v2.6.36		y	got from 32-stable
e5aec52d 	068e35ee	v2.6.36		n
96b6a8c5 	xxxxxxxx	-- ? -- 	?
1252894f 	xxxxxxxx	-- ? -- 	?
6d51cdff 	xxxxxxxx	-- ? -- 	?
ba4de7fd 	6df7aadc	v2.6.36		n
d4f5d4c1 	65745422	v2.6.36		n
d987d040 	31c4a3d3	v2.6.36		n
69bce8b4 	4969c119	v2.6.36		n
5b11eef8 	339db11b	v2.6.36		y	got from 32-stable
9db783be 	dd173abf	v2.6.36		y	got from 32-stable
b38ac5ef 	ab12811c	v2.6.36		y	got from 32-stable
073b0b42 	44467187	v2.6.36		y	got from 32-stable
79872c19 	49c37c03	v2.6.36		y	got from 32-stable
9df2a1a9 	7011e660	v2.6.36		y	got from 32-stable
df7b44d7 	25edd694	v2.6.36		y	got from 32-stable
1919a55f 	ae2688d5	v2.6.36		y	got from 32-stable
2974d512 	bfc960a8	v2.6.36		n
4aeda42a 	a9117426	???????		n
749f63ed 	719f8358	v2.6.36		n
4275a88a 	01f83d69	v2.6.36		y	got from 32-stable
5bc7298f 	d84ba638	v2.6.36		y	got from 32-stable
8411b2de 	c5ed63d6	v2.6.36		y	got from 32-stable
fd31b5dc 	ad1af0fe	v2.6.36		y	got from 32-stable
515b723e 	6dcbc122	v2.6.36		n
3289b85e 	f037590f	v2.6.36		y	got from 32-stable
2f2c0c64 	628e300c	v2.6.36		y	got from 32-stable
88c2651d 	64289c8e	v2.6.36		y	got from 32-stable
76dcdc68 	3d3be433	v2.6.36		y	got from 32-stable
5d92da44 	4ce6b9e1	???????		n
150e749a 	a0846f18	v2.6.36		y	got from 32-stable
6c09969e 	024cfa59	v2.6.36		n
---- v2.6.35.7 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
ea8a52f9 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
b0dd220a 	xxxxxxxx	-- ? -- 	?
---- v2.6.35.8 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
f16e6e4d 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
5daf133f 	39aa3cb3	v2.6.36		y	got from 32-stable
e4037861 	9aea5a65	v2.6.36		y	got from 32-stable
c7d3b641 	7993bc1f	v2.6.36		y	got from 32-stable
a376eaa8 	1b528181	v2.6.36		y	got from 32-stable
39f47702 	b7d46089	v2.6.36		n
d521d5c5 	8848a910	v2.6.36		n
ba8f2de5 	fd89a137	v2.6.36		n
c4cf17f1 	f8f235e5	v2.6.36		n
6b85aae5 	1dedefd1	v2.6.36		y	got from 32-stable
fc5ae349 	53998648	v2.6.36		n
fe1a1f15 	337279ce	v2.6.36		y	got from 32-stable
6d83ebff 	64a32307	v2.6.36		y	got from 32-stable
223c90ef 	7a1d602f	v2.6.36		y	got from 32-stable
00f8646e 	4731fdcf	v2.6.36		y	got from 32-stable
e5668aa8 	573b6381	v2.6.36		y	got from 32-stable
6fe32940 	f7154de2	v2.6.36		n
073676f1 	bcf64aa3	v2.6.36		y	got from 32-stable
7837447f 	54a83404	v2.6.36		y	got from 32-stable
02f6f1e6 	f761622e	v2.6.36		y	got from 32-stable
50c5f9de 	c1e0ddbf	v2.6.36		n
64b4822a 	531295e6	v2.6.36		n
30756523 	16d3ea26	v2.6.36		n
3899bc97 	47897160	v2.6.36		y	got from 32-stable
8706a900 	ca242ac9	v2.6.36		n
85aa4285 	6bbfb265	v2.6.36		n
d25faf32 	47008cd8	v2.6.36		y	got from 32-stable
912ecf41 	58877679	v2.6.36		y	got from 32-stable
668f36c4 	eebb5f31	v2.6.36		n
431e8d4b 	aeb19f60	v2.6.36		y	got from 32-stable
2af177aa 	392bd0cb	v2.6.36		y	got from 32-stable
4d4e307a 	081003ff	v2.6.36		n
47f4633f 	90e12cec	v2.6.36		n
63cb57e4 	1c8cf9c9	v2.6.36		n
414c9c27 	f36fce0f	v2.6.36		n
0097ad30 	e488459a	v2.6.36		n
f68f4a26 	73758a5d	v2.6.36		n
86e3fc71 	xxxxxxxx	-- ? -- 	?
64ea843f 	f459ffbd	v2.6.36		y	got from 32-stable
9b909fc8 	39b4d07a	v2.6.36		n
ac9f3719 	31dfbc93	v2.6.36		n
39a3fcd4 	d270ae34	v2.6.36		n
264cf4e6 	ce9d419d	v2.6.36		n
f7352937 	6939a5ac	v2.6.36		n
14d7e74a 	aa0170ff	v2.6.36		n
fe17f612 	799c1055	v2.6.36		y	got from 32-stable
c578c131 	6dcbfe4f	v2.6.36		y	got from 32-stable
75a0ef0b 	73cf624d	v2.6.36		n
6c4a46ec 	ec5a32f6	v2.6.36		y	got from 32-stable
7ca97834 	df6d0230	v2.6.36		y	got from 32-stable
ad2f6a9c 	c9d66d35	v2.6.36		n
f4604440 	1fc8a117	v2.6.36		y	got from 32-stable
5ecce9b3 	9d8117e7	v2.6.36		n
cd1bbdfe 	3f259d09	v2.6.36		n
73875793 	6abb930a	v2.6.36		y	got from 32-stable
eafe9c35 	f13d4f97	v2.6.36		y	got from 32-stable
6c05813f 	929f49bf	v2.6.36		n
677c3a48 	cc60f887	v2.6.36		y	got from 32-stable
02ee32ed 	d0134324	v2.6.36		y	got from 32-stable
39a1d13e 	e4c4776d	v2.6.36		n
c4cb1dd9 	d4cfa4d1	v2.6.36		n
84602dcc 	47526903	v2.6.36		y	got from 32-stable
6af2c5f5 	1cf180c9	v2.6.36		y	got from 32-stable
8268a8d7 	02198962	v2.6.36		y	got from 32-stable
22d59a19 	cd87a2d3	v2.6.36		n
b6df308f 	c50a898f	v2.6.36		n
df2b79cc 	ca047fed	v2.6.36		n
1a93a2c2 	d2520a42	v2.6.36		y	got from 32-stable
81e8f583 	3bfb317f	v2.6.36		n
06deee8c 	e0172fd3	v2.6.36		n
e530826e 	c10469c6	v2.6.36		y	got from 32-stable
d7d8fdce 	3e645d6b	v2.6.36		y	got from 32-stable
6cd8e580 	64aab720	v2.6.36		n
3a50feec 	4e31635c	v2.6.36		n
b2d83273 	258af474	v2.6.36		y	got from 32-stable
f574ccce 	9ecd4e16	v2.6.36		y	got from 32-stable
1cd864ee 	4c894f47	v2.6.36		y	got from 32-stable
f686b1e0 	04e0463e	v2.6.36		y	got from 32-stable
b7dfcb6f 	e9bf5197	v2.6.36		y	got from 32-stable
0afe0f1a 	9320f7cb	v2.6.36		y	got from 32-stable
74352c5c 	04d174e9	v2.6.36		n
a035671a 	882787ff	v2.6.36		n
c1e962e4 	1a8e41cd	v2.6.36		n
134da12c 	79e27dc0	v2.6.36		n
895384a4 	6a2a11db	v2.6.36		n
786c022a 	d31dba58	v2.6.36		n
b8a7e2b6 	bec658ff	v2.6.36		y	got from 32-stable
1b6740a1 	98d943b0	v2.6.36		n
b3d8659d 	bb7ab785	v2.6.36		y	got from 32-stable
cc64346c 	100cf877	v2.6.36		n
a7b048db 	f015ac3e	v2.6.36		n
51793adc 	fad16e7a	v2.6.36		n
cdcaefbc 	e42dee9a	v2.6.36		n
dc7237c6 	d20d5ffa	v2.6.36		n
6ce3cb68 	a666e3e6	v2.6.36		y	got from 32-stable
c9c7a50d 	bd2e74d6	v2.6.36		y	got from 32-stable
366caa1d 	0026e005	v2.6.36		y	got from 32-stable
085835ef 	f7c77a3d	v2.6.36		n
105f55f7 	aa73aec6	v2.6.36		y	got from 32-stable
b81cb927 	5591bf07	v2.6.36		y	got from 32-stable
e5d843fb 	0f9f1ee9	v2.6.36		y	got from 32-stable
a09d28ae 	0873a5ae	v2.6.36		y	got from 32-stable
292a4537 	e68d3b31	v2.6.36		y	got from 32-stable
9f09ac68 	d900329e	v2.6.36		y	got from 32-stable
---- v2.6.35.9 ----
Commit		Parent		Parent in	In 34.x		Notes
========================================================================
512ac859 	xxxxxxxx	-- ? -- 	n/a	Makefile ver. change
4c3c3a11 	16518d5a	v2.6.36		n
e3a85508 	bc10f967	v2.6.36		n
29d818b4 	09358972	v2.6.37		y	got from 32-stable
2a883ff9 	xxxxxxxx	-- ? -- 	?
e0b20e11 	15714f7b	v2.6.37		n
badaad1d 	94e22389	v2.6.36		y	got from 32-stable
e26158f9 	d2ed8177	v2.6.37		n
3f266b04 	xxxxxxxx	-- ? -- 	?
a3c1c570 	db5a753b	v2.6.37		n
4ac86dc5 	8c974438	v2.6.37		n
ea912fae 	4d22f7d3	v2.6.36		n
4ba3d9ba 	a4d25803	v2.6.36		y	got from 32-stable
56b8700a 	xxxxxxxx	-- ? -- 	?
44f89254 	482964e5	v2.6.36		y	got from 32-stable
a93a9798 	9828e6e6	v2.6.36		y	got from 32-stable
ebe60f32 	e2269308	v2.6.37		n
2556c3ee 	a91e7d47	v2.6.36		y	got from 32-stable
f38eaf1f 	8df8fd27	v2.6.36		n
cf557ed8 	7e96dc70	v2.6.36		y	got from 32-stable
971962be 	ae878ae2	v2.6.36		y	got from 32-stable
03b965ed 	801715f9	???????		n
f75c81af 	3d13008e	v2.6.36		y	got from 32-stable
4d7fa13e 	0d1fe111	???????		n
d2764500 	9756403b	v2.6.37		n
d6a94163 	b00916b1	v2.6.36		y	got from 32-stable
01660e27 	986fe6c7	v2.6.37		y	got from 32-stable
6ed019fa 	546ae796	v2.6.37		y	got from 32-stable
5934df9b 	f63ae56e	v2.6.37		y	got from 32-stable
77bfbe19 	f0ad30d3	v2.6.37		y	got from 32-stable
c5b2751c 	1a03ae0f	v2.6.37		y	got from 32-stable
616ab3c9 	23f45c3a	v2.6.37		n
21a0f6c9 	56626a72	v2.6.37		y	got from 32-stable
3952e7d5 	ac9dfe9c	v2.6.37		n
973d9881 	97cd8dc4	v2.6.37		y	got from 32-stable
867b3d87 	cfb8da8f	v2.6.37		n
554521a6 	80f0cf39	v2.6.37		y	got from 32-stable
d0aed2ec 	969affff	v2.6.37		y	got from 32-stable
5fc8fe8e 	93ad03d6	v2.6.37		y	got from 32-stable
303dde70 	2f1136d1	v2.6.37		y	got from 32-stable
fde2940f 	ecfa153e	v2.6.37		y	got from 32-stable
86492f3c 	5c8db070	v2.6.37		n
b6646b62 	00be545e	v2.6.37		y	got from 32-stable
95112caf 	3daad24d	v2.6.37		n
5b6ebed8 	f4053874	v2.6.37		n
3c95a4f9 	0f266abd	v2.6.37		y	got from 32-stable
af470176 	59c6ccd9	v2.6.37		y	got from 32-stable
b36f1df6 	99c1e4f8	v2.6.37		y	got from 32-stable
6f5fd62a 	677aeafe	v2.6.37		n
da768c0b 	3126d823	v2.6.37		y	got from 32-stable
5aaade9e 	1c6529e9	v2.6.37		n
70cb495a 	ba0534be	v2.6.37		n
8f8ed8cb 	5c836e4d	v2.6.37		n
185de918 	0d91f22b	v2.6.37		y	got from 32-stable
4ffa3ba4 	1a92795d	v2.6.37		y	got from 32-stable
7522ac94 	11791a6f	v2.6.37		y	got from 32-stable
ea8ef5f0 	e5953cbd	v2.6.37		y	got from 32-stable
5ed216a9 	92ca0dc5	v2.6.37		n
fb84c60f 	cda00082	v2.6.37		n
c0b67df5 	4c62a2dc	v2.6.37		n
b78891df 	6554287b	v2.6.37		n
d59159c6 	37a2f9f3	v2.6.37		y	got from 32-stable
79259785 	75e3cfbe	v2.6.37		y	got from 32-stable
548bf358 	3fdbf004	v2.6.37		y	got from 32-stable
ce04bd41 	286e5b97	v2.6.37		y	got from 32-stable
644b2192 	76fac077	v2.6.37		y	got from 32-stable
8298c02a 	7ef8aa72	v2.6.37		y	got from 32-stable
ff7d2317 	3ee48b6a	v2.6.37		y	got from 32-stable
db11393d 	c25d2995	v2.6.37		n
4fc91067 	6ad60195	v2.6.37		n
28feaa47 	aa91c7e4	v2.6.37		n
52c974f3 	7ada876a	v2.6.37		y	got from 32-stable
46ee0239 	c19483cc	v2.6.37		y	got from 32-stable
ee6158ba 	7740191c	v2.6.37		y	got from 32-stable
19b0be98 	17bdcf94	v2.6.37		n
a02c10c1 	5b917a14	v2.6.37		y	got from 32-stable
49a9147e 	9f5f9ffe	v2.6.37		y	got from 32-stable
c119599e 	c530ccd9	v2.6.37		n
cef2c20c 	ba0cef3d	v2.6.37		n
2a64ff43 	cf10700b	v2.6.37		n
0fa89f1c 	584c5b7c	v2.6.37		y	got from 32-stable
12b1e06a 	0c9a32f0	v2.6.37		y	got from 32-stable
